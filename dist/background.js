chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
	chrome.declarativeContent.onPageChanged.addRules([
		{
			conditions: [
				new chrome.declarativeContent.PageStateMatcher({
					pageUrl: { hostEquals: "cmsweb.cms.csulb.edu" }
				})
			],
			actions: [new chrome.declarativeContent.ShowPageAction()]
		}
	]);
});
