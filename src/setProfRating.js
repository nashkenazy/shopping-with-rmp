import { stringSimilarity } from "string-similarity-js";
import tippy from "tippy.js";
// Fetch data
let rmpArray = [];
const fetchRmpJson = async () =>
  await (await fetch(
    "https://search.mtvnservices.com/typeahead/suggest/?rows=10000&q=*%3A*+schoolid_s%3A162&siteName=rmp&fl=pk_id+teacherfirstname_t+teacherlastname_t+total_number_of_ratings_i+averageratingscore_rf&fq="
  )).json();

fetchRmpJson()
  .then(data => (rmpArray = data.response.docs))
  .catch(reason => console.log(reason.message));

// Populate slots
const setProfRating = slot => {
  if (slot.id && slot.id.indexOf("MTG_INSTR") == 0) {
    const fullName = slot.innerHTML;
    const fullNameArray = fullName.split(" ");
    const firstName = fullNameArray[0];
    const lastName = fullNameArray[fullNameArray.length - 1];
    let profIndex = findProfIndex(firstName, lastName, fullName);
    let likelyProfSimilarity = 0;
    if (profIndex === "notFound") {
      const likelyProfData = findLikelyProfData(fullName);
      profIndex = likelyProfData[0];
      likelyProfSimilarity = likelyProfData[1];
    }
    let prof = rmpArray[profIndex];
    if (prof) {
      editProfSlot(prof, slot, likelyProfSimilarity);
    }
  }
};

const findProfIndex = (firstName, lastName, fullName) => {
  if (fullName === "Staff") {
    return -1;
  }
  for (var i = 0; i < rmpArray.length; i++) {
    if (rmpArray[i]["teacherfirstname_t"] == firstName && rmpArray[i]["teacherlastname_t"] == lastName) {
      return i;
    }
  }
  return "notFound";
};

const findLikelyProfData = fullName => {
  let likelyProfIndex = 0;
  let likelyProfScore = 0;
  for (var i = 0; i < rmpArray.length; i++) {
    const fullNameDB = `${rmpArray[i].teacherfirstname_t} ${rmpArray[i].teacherlastname_t}`;
    const similarityScore = stringSimilarity(fullName, fullNameDB);
    if (likelyProfScore < similarityScore) {
      likelyProfScore = similarityScore;
      likelyProfIndex = i;
    }
  }
  return [likelyProfIndex, likelyProfScore];
};

const editProfSlot = (prof, slot, likelyProfSimilarity) => {
  const a = document.createElement("a");
  a.href = "http://www.ratemyprofessors.com/ShowRatings.jsp?tid=" + prof.pk_id;
  slot.appendChild(a).appendChild(a.previousSibling);

  const profScores = document.createElement("div");
  if (prof.total_number_of_ratings_i > 0) {
    profScores.innerHTML =
      "Rating: " + prof.averageratingscore_rf + "/5" + "<br />" + prof.total_number_of_ratings_i + " reviews";
    if (prof.averageratingscore_rf > 3.8) {
      slot.parentElement.style.backgroundColor = "#d9f2d9";
    } else if (prof.averageratingscore_rf <= 3.8 && prof.averageratingscore_rf >= 2.5) {
      slot.parentElement.style.backgroundColor = "#faecd1";
    } else {
      slot.parentElement.style.backgroundColor = "#ffcccc";
    }
    if (likelyProfSimilarity > 0) {
      slot.parentElement.style.backgroundColor = "#d2eff9";
      const profName = `${prof["teacherfirstname_t"]} ${prof["teacherlastname_t"]}`;
      const roundedSimilarityPercent = Math.round(likelyProfSimilarity * 100);
      slot.parentElement.title = roundedSimilarityPercent + "% similar to " + profName;
      tippy(slot.parentElement);
    }
  } else {
    profScores.innerHTML = "No Reviews";
  }
  slot.insertAdjacentElement("afterend", profScores);
};

export default setProfRating;
