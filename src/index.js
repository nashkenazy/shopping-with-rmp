import setProfRating from "./setProfRating";
import setScheduleAvailability from "./setScheduleAvailability";

let optionsStore = {};
let pastCartOnce = false; // Assigned globally because modifySearchResult runs twice due to the node.
let hasCart = false;
let isEnrolled = false;

chrome.storage.sync.get(null, storageObject => {
  optionsStore = storageObject;
});

// Detects DOM changes then runs because loading the search engine does not update the URL.
MutationObserver = window.MutationObserver || window.WebKitMutationObserver;

let catalogLoaded = new MutationObserver(() => {
  const showCartButton = document.getElementById("DERIVED_SSS_CRT_SSS_SHOW_ALL");
  if (showCartButton) {
    hasCart = true;
    showCartButton.click();
  }
  modifySearchResult.observe(document.getElementById("WAIT_win0"), {
    subtree: true,
    attributes: true
    //...
  });
});

let modifySearchResult = new MutationObserver(() => {
  if (document.getElementById("WAIT_win0").style.visibility == "hidden" && !pastCartOnce) {
    pastCartOnce = true;
    const showEnrolledButton = document.getElementById("DERIVED_SSS_ENR_SSS_SHOW_ALL");
    if (showEnrolledButton) {
      showEnrolledButton.click();
      isEnrolled = true;
    }
    enrolledLoaded.observe(document.getElementById("WAIT_win0"), {
      subtree: true,
      attributes: true
      //...
    });
  }
});

let enrolledLoaded = new MutationObserver(() => {
  setGuide(hasCart, isEnrolled);
  // Disconnect so that the cart openings can happen again when the user searches again.
  modifySearchResult.disconnect();
  enrolledLoaded.disconnect();
  // Reset values in case of changes.
  pastCartOnce = false;
  hasCart = false;
  isEnrolled = false;
});

catalogLoaded.observe(document.getElementById("pt_pageinfo_win0"), {
  subtree: true,
  attributes: true
  //...
});

const setGuide = (hasCart, isEnrolled) => {
  let spans = document.getElementsByTagName("span");
  for (let slot of spans) {
    setScheduleAvailability(slot, hasCart, isEnrolled);
    setProfRating(slot);
  }
};
