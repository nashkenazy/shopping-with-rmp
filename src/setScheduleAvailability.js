import tippy from "tippy.js";

let enrolledTimes = { Mo: [], Tu: [], We: [], Th: [], Fr: [], Sa: [], Su: [] };
let reservedTimes = { Mo: [], Tu: [], We: [], Th: [], Fr: [], Sa: [], Su: [] };
let slotIdNumber = "";

const setScheduleAvailability = (slot, hasCart, isEnrolled) => {
  fillTakenTimes(slot, hasCart, isEnrolled);

  if (slot.id && slot.id.indexOf("MTG_DAYTIME") == 0) {
    const scheduleSlot = slot.innerHTML.split(" ");
    if (scheduleSlot[0] === "TBA") {
      return;
    }
    const daysArray = scheduleSlot[0].split(/(?=[A-Z])/);
    const startTime = dirtyTimeToMinutes(scheduleSlot[1]);
    const endTime = dirtyTimeToMinutes(scheduleSlot[3]);
    slot.parentElement.style.backgroundColor = "#d9f2d9";
    for (let day of daysArray) {
      const enrolledSlotData = clashingClassData(enrolledTimes, day, startTime, endTime);
      const enrolledSlotId = enrolledSlotData[0];
      const enrolledSlotSchedule = enrolledSlotData[1];
      const reservedSlotData = clashingClassData(reservedTimes, day, startTime, endTime);
      const reservedSlotId = reservedSlotData[0];
      const reservedSlotSchedule = reservedSlotData[1];
      if (
        handleScheduleHover(enrolledSlotId, slot, "ENR", enrolledSlotSchedule) ||
        handleScheduleHover(reservedSlotId, slot, "CRT", reservedSlotSchedule)
      ) {
        break;
      }
    }
  }
};

const fillTakenTimes = (slot, hasCart, isEnrolled) => {
  if (
    (slot.id && slot.id.indexOf("DERIVED_SSS_ENR_SSR_MTG_SCHED_LONG") == 0) ||
    (slot.id && slot.id.indexOf("DERIVED_SSS_CRT_SSR_MTG_SCHED_LONG") == 0)
  ) {
    let scheduleSlot = slot.innerHTML.split(" ");
    scheduleSlot.length = 4;

    const daysArray = scheduleSlot[0].split(/(?=[A-Z])/);
    const startTime = dirtyTimeToMinutes(scheduleSlot[1]);
    const endTime = dirtyTimeToMinutes(scheduleSlot[3].substring(0, scheduleSlot[3].indexOf("<br")));

    if (slot.id && slot.id.indexOf("DERIVED_SSS_ENR_SSR_MTG_SCHED_LONG") == 0 && isEnrolled) {
      slotIdNumber = slot.id[slot.id.length - 1];
      daysArray.forEach(
        day => (enrolledTimes[day] = [...enrolledTimes[day], [slotIdNumber, startTime, endTime, scheduleSlot]])
      );
    } else if (hasCart) {
      slotIdNumber = slot.id[slot.id.length - 1];
      daysArray.forEach(
        day => (reservedTimes[day] = [...reservedTimes[day], [slotIdNumber, startTime, endTime, scheduleSlot]])
      );
    }
  }
};

const dirtyTimeToMinutes = time12h => {
  time12h = time12h.substr(0, time12h.length - 2) + " " + time12h.substr(time12h.length - 2);
  const [time, modifier] = time12h.split(" ");
  let [hours, minutes] = time.split(":");

  if (hours === "12") {
    hours = "00";
  }

  if (modifier === "PM") {
    hours = parseInt(hours, 10) + 12;
  }

  return 60 * hours + parseInt(minutes, 10);
};

const clashingClassData = (plannedTimes, day, startTime, endTime) => {
  for (var i = 0; i < plannedTimes[day].length; i++) {
    const slotIdNumber = plannedTimes[day][i][0];
    const takenStartTime = plannedTimes[day][i][1];
    const takenEndTime = plannedTimes[day][i][2];
    const dirtyScheduleSlot = plannedTimes[day][i][3].join();
    const scheduleSlot = dirtyScheduleSlot
      .split(",")
      .join(" ")
      .substring(0, dirtyScheduleSlot.indexOf("<br"));

    if (
      (startTime >= takenStartTime && startTime <= takenEndTime) ||
      (endTime >= takenStartTime && endTime <= takenEndTime)
    ) {
      return [slotIdNumber, scheduleSlot];
    }
  }
  return [false, false];
};

const handleScheduleHover = (takenSlotId, slot, type, takenSlotSchedule) => {
  if (takenSlotId === false) {
    return false;
  }
  let clashingClass = document.getElementById("DERIVED_SSS_" + type + "_SSS_SUBJ_CATLG$" + takenSlotId).innerHTML;

  // Checks if there's an empty slot of &nbsp;
  while (clashingClass.indexOf("&nbsp") > -1) {
    // Go up until course is found
    takenSlotId--;
    clashingClass = document.getElementById("DERIVED_SSS_" + type + "_SSS_SUBJ_CATLG$" + takenSlotId).innerHTML;
  }

  if (type === "ENR") {
    slot.parentElement.style.backgroundColor = "#ddd";
    slot.parentElement.title = "Enrolled: " + clashingClass + " at " + takenSlotSchedule;
  } else {
    slot.parentElement.style.backgroundColor = "#faecd1";
    slot.parentElement.title = "Cart: " + clashingClass + " at " + takenSlotSchedule;
  }
  tippy(slot.parentElement);
  return true;
};

export default setScheduleAvailability;
